#!/bin/bash

usage() {
cat << EOF
Usage: $( basename $0 ) [-h] [-p] [-r] -i INFILE

Prints linenumbers from INFILE where a line contains at least
3 consecutive alphabetic characters (from the Unicode range).

  -h            show this help message and exit
  -i INFILE     file to operate on
  -r            reverse: print lines that do NOT
                contain any alphabetic char
EOF
}

INFILE=
REV=false

while getopts "hi:r" opt; do
    case ${opt} in
        h)
            usage
            exit 1
            ;;
        i)
            INFILE="${OPTARG}"
            ;;
        r)
            REV=true
            ;;
        ?)
            usage
            exit
            ;;
    esac
done

# if INFILE variable is an empty string
if [[ -z "${INFILE}" ]]; then
    usage
    exit 1
fi


# normal operation
if [ "${REV}" = false ]; then
    grep --no-messages --line-number '[[:alpha:]][[:alpha:]][[:alpha:]]' "${INFILE}" \
        | awk -F: '{print $1}'
# else reverse
else
    grep --no-messages --line-number -v '[[:alpha:]][[:alpha:]][[:alpha:]]' "${INFILE}" \
        | awk -F: '{print $1}'
fi
