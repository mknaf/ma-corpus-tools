# About
A collection of tools to help build a subcorpus from another
corpus given some keywords. Call the scripts with `-h` switch
for usage instructions.
