#!/bin/bash

for file in $@; do
    OUTFILE=$(echo ${file} | sed 's/tok/detok/g')
    ~/magister/moses/mosesdecoder/scripts/tokenizer/detokenizer.perl -l en < "${file}" > "${OUTFILE}"
done
