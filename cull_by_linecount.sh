#!/bin/bash
#

usage() {
cat << EOF
Usage: $( basename $0 ) -i INDIR -o OUTDIR -n NUM

Copy all files with less than NUM lines from INDIR to OUTDIR.

  -h            show this help message and exit
  -i INDIR      directory to read files from
  -o OUTDIR     directory to write files to
  -n NUM        max number of lines in file
EOF
}

INDIR=
OUTDIR=
NUM=

while getopts "hi:o:n:" opt; do
    case ${opt} in
        h)
            usage
            exit 1
            ;;
        i)
            INDIR=${OPTARG}
            ;;
        o)
            OUTDIR=${OPTARG}
            ;;
        n)
            NUM=${OPTARG}
            ;;
        ?)
            usage
            exit
            ;;
    esac
done

if [[ -z ${INDIR} ]] \
    || [[ -z ${OUTDIR} ]] \
    || [[ -z ${NUM} ]] \
    || [[ ! ${NUM} =~ ^[0-9]+$ ]]; then
    usage
    exit 1
fi

# make sure the ouput dir exists
mkdir -p ${OUTDIR}

# for all the files in INDIR
for file in "${INDIR}"/*; do
    # find out how many lines are in this file
    flen=$( wc -l "${file}" | awk '{print $1}')

    # if there are less than ${NUM} lines in the file
    if [[ ${flen} -le ${NUM} ]]; then

        # copy the file to ${OUTDIR}
        cp "${file}" "${OUTDIR}"/
    fi
done
