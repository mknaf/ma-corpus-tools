#!/bin/bash

usage() {
cat << EOF
Usage: $(basename $0) [-h] [-r] -s SRC_CORP -t TRG_CORP -o S|T

Output to STDOUT corpus/bitext-wide uniqe lines (src and trg) for
a given SRC_CORP and TRG_CORP. Will only output one side (S or T)
at one time.

If you get weird results, try to change the delimiter.

  -h            show this help message and exit
  -s SRC_CORP   the source side of the corpus
  -t TRG_CORP   the target side of the corpus
  -o S|T        which side to output, source or target. Use uppercase letters.
  -r            reverse: shuffle instead of sort and uniq. this will
                    output a bitext split by \$DELIM that you have to
                    split manually.

EOF
}


SRC_CORP=
TRG_CORP=
OUT=
REV=false

# read options
while getopts "hs:t:o:r" opt; do
    case ${opt} in
        h)
            usage
            exit
            ;;
        s)
            SRC_CORP="${OPTARG}"
           ;;
        t)
            TRG_CORP="${OPTARG}"
            ;;
        o)
            OUT="${OPTARG}"
            ;;
        r)
            REV=true
            ;;
        ?)
            usage
            exit
            ;;
    esac
done

# check that we weren't given empty strings
if [[ -z "${SRC_CORP}" ]] \
    || [[ -z "${TRG_CORP}" ]] \
    || [[ -z "${OUT}" ]]; then
    usage
    exit 1
fi

# decide which side to output
if [[ "S" == "${OUT}" ]]; then
    OUT=1
elif [[ "T" == "${OUT}" ]]; then
    OUT=2
else
    usage
    exit 1
fi


DELIM='______'

# normal operation
if [ "${REV}" = false ]; then
    # paste DELIM between two lines,
    # sort and unique,
    # split at DELIM,
    # print S or T side
    paste -d "${DELIM}" "${SRC_CORP}" - - - - - "${TRG_CORP}" < /dev/null \
        | sort \
        | uniq \
        | awk -F"${DELIM}" "{print \$${OUT}}"
else
    paste -d "${DELIM}" "${SRC_CORP}" - - - - - "${TRG_CORP}" < /dev/null \
        | shuf
fi
