#!/bin/bash

usage() {
cat << EOF
Usage: $( basename $0 ) [-h] -i INFILE -n MAX_LEN

Write to STDOUT all the linenumbers from INFILE which
have less than MAX_LEN words.

  -h            show this help message and exit
  -i INFILE     file with the lines to check for length
  -n MAX_LEN    max length (wordcount) of a line

EOF
}

INFILE=
MAX_LEN=

# read options values
while getopts "hi:n:" opt; do
    case ${opt} in
        h)
            usage
            exit
            ;;
        i)
            # due to the while loop, we need to convert the '-'
            # explicitly to '/dev/stdin', otherwise it will be
            # treated as a filename inside the loop
            if [ "${OPTARG}" = "-" ]; then
                INFILE="/dev/stdin"
            else
                INFILE="${OPTARG}"
            fi
            ;;
        n)
            MAX_LEN="${OPTARG}"
            ;;
        ?)
            usage
            exit
            ;;
    esac
done

if [[ -z "${INFILE}" ]] || [[ -z "${MAX_LEN}" ]]; then
    usage
    exit 1
fi


check_lengths() {
    local i=1

    # for each line in INFILE
    while read line; do

        # if the line has less words than MAX_LEN
        if [ "$(echo ${line} | wc -w )" -lt "${MAX_LEN}" ]; then
            # echo that linenumber to STDOUT
            echo $i
        fi

        # increment counter
        i=$(( $i + 1 ))

    done < "${INFILE}"
}

check_lengths
