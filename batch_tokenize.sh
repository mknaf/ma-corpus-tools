#!/bin/bash

for file in $@; do
    ~/magister/moses/mosesdecoder/scripts/tokenizer/tokenizer.perl -l de < "${file}" > "${file}".tok
done
