#!/bin/bash

usage() {
cat << EOF
Usage: $(basename $0) [-h] [-e] [-p] -i INFILE

Strip leading digits off all lines in INFILE that are
followed by an uppercase word and write result to STDOUT.

  -h            show this help message end exit
  -i INFILE     the file from which to read
  -p            edit the file in place instead of writing to STDOUT
  -e            show some examples

The script can act a little too enthusiastic, see examples.

EOF
}

example() {
cat << EOF

Will strip for example:

    404 Überschrift
        -> Überschrift
    007 James Bond
        -> James Bond
    10 Bier für die Männer vom Sägewerk
        -> Bier für die Männer vom Sägewerk (bad!)

Will not strip:

    10% of all the people
    10 ml
    1.5 kg
    10MB disk space
    ...

EOF
}

INFILE=
IN_PLACE=

while getopts "hei:p" opt; do
    case ${opt} in
        h)
            usage
            exit
            ;;
        e)
            example
            exit
            ;;
        i)
            INFILE="${OPTARG}"
            ;;
        p)
            IN_PLACE="-i"
            ;;
        ?)
            usage
            exit;;
    esac
done

if [[ -z "${INFILE}" ]]; then
    usage
    exit 1
fi


sed "${IN_PLACE}" 's/^\([0-9]\+ \)\([[:upper:]].*\)/\2/g' "${INFILE}"
