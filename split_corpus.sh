#!/bin/bash
#
# Helper to split corpora into desired sizes.

usage()
{
cat << EOF
Usage : split.sh --test-size NUM --tune-size NUM --corpus-file INFILE [--train-size NUM] [--outdir DIR]

Splits a corpus file like europarl into chunks of desired sizes.

  --test-size NUM       number of lines to end up in the testing corpus
  --tune-size NUM       number of lines to end up in the tuning corpus
  --corpus-file INFILE
                        file from which to get the lines
  --train-size NUM      optional: number of lines to end up in the training
                        corpus. If not given, uses all remaining lines.
  --outdir DIR          optional: where to write resulting files. If
                        not given, writes to the directory of the
                        source file.

EOF
}

# we need number of arguments * 2 because of the values
if [[ $# -lt 6 ]]; then
    usage
    exit
fi

# find the values we're interested in in the argument string
while [[ $# > 1 ]]; do
    key="$1"
    case $key in
        --tune-size)
            SIZE_TUNE="$2"
            shift
            ;;
        --test-size)
            SIZE_TEST="$2"
            shift
            ;;
        --train-size)
            SIZE_TRAIN="$2"
            shift
            ;;
        --corpus-file)
            CORP="$2"
            shift
            ;;
        --outdir)
            OUTDIR="$2"
            shift
            ;;
        *)
            # unknown option
            ;;
    esac
    shift
done

# check if we got our required arguments
if [ -z "${SIZE_TEST}" ]; then
    usage
    exit 1
fi
if [ -z "${SIZE_TUNE}" ]; then
    usage
    exit 1
fi
if [ ! -f "${CORP}" ]; then
    usage
    echo
    echo "Error: file '${CORP}' not found."
    exit 1
fi

# if we didn't get outdir
if [ -z "${OUTDIR}" ]; then
    # output to the same directory as the input corpus is
    OUTDIR=$(dirname ${CORP})
# else if outdir was passed
else
    # make sure outdir exists
    mkdir -p "${OUTDIR}"
fi

# check if the sizes are actually numbers
if [[ ! "${SIZE_TEST}" =~ ^[0-9]+$ ]]; then
    usage
    exit 1
fi
if [[ ! "${SIZE_TUNE}" =~ ^[0-9]+$ ]]; then
    usage
    exit 1
fi

# check if there are enough lines in the source corpus
SIZE_CORP=$(wc -l "${CORP}" | awk '{print $1}')
SIZE_OUT="$(( ${SIZE_TRAIN} + ${SIZE_TEST} + ${SIZE_TUNE} ))"
if [ "${SIZE_OUT}" -gt "${SIZE_CORP}" ]; then
    usage
    echo
    echo "Error: You want more lines in your output files than are available"
    echo "in the source. Please think about that long and hard, then retry."
    exit 1
fi


# make test corpus
BEGIN_TEST=1
END_TEST=${SIZE_TEST}
sed -n ${BEGIN_TEST},${END_TEST}p "${CORP}" > "${OUTDIR}"/$(basename "${CORP}").test

# make tune corpus
BEGIN_TUNE=$(( ${END_TEST} + 1 ))
END_TUNE=$(( ${SIZE_TUNE} + ${SIZE_TEST} ))
sed -n ${BEGIN_TUNE},${END_TUNE}p ${CORP} > "${OUTDIR}"/$(basename "${CORP}").tune

# make train corpus
BEGIN_TRAIN=$(( ${END_TUNE} + 1 ))
# if train-size was not given
if [ -z "${SIZE_TRAIN}" ]; then
    # set end of training corpus to end of source corpus
    END_TRAIN=${SIZE_CORP}
# else train-size was given
else
    # set end of training corpus to size_test + size_tune + size_train
    END_TRAIN=$(( ${SIZE_TUNE} + ${SIZE_TEST} + ${SIZE_TRAIN} ))
fi
sed -n ${BEGIN_TRAIN},${END_TRAIN}p ${CORP} > "${OUTDIR}"/$(basename "${CORP}").train
