#!/bin/bash

#REF="${HOME}/magister/corpus/EMEA/EMEA.shuf.de-en.en.test.tok"
REF="${HOME}/magister/corpus/pattr/pattr/abstract/pattr.1mio.de-en.en.test.tok.esc"

for file in $@; do
    echo "${file}"
    SCORE=$(~/magister/moses/mosesdecoder/scripts/generic/multi-bleu.perl -lc "${REF}" < "${file}")
    echo $SCORE
    echo
done
