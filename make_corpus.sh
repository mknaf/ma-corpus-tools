#!/bin/bash

usage() {
cat << EOF
Usage: $(basename $0) [-h] -i INFILE -o OUTFILE -l LINENUMS -r

Take LINENUMS lines from INFILE and write them to OUTFILE.

  -h            show this help message and exit
  -i INFILE     input file
  -o OUTFILE    output file
  -l LINENUMS   a file containing line numbers
  -r            reverse - make a corpus that does NOT have LINENUMS

EOF
}

INFILE=
OUTFILE=
LINENUMS=

# read argument list
while getopts "hi:o:l:r" opt; do
    case ${opt} in
        h)
            usage
            exit
            ;;
        i)
            INFILE="${OPTARG}"
            ;;
        o)
            OUTFILE="${OPTARG}"
            ;;
        l)
            LINENUMS="${OPTARG}"
            ;;
        r)
            REV=true
            ;;
        ?)
            usage
            exit
            ;;
    esac
done

# check that we weren't passed empty strings
if [[ -z "${INFILE}" ]] || [[ -z "${OUTFILE}" ]] || [[ -z "${LINENUMS}" ]]; then
    usage
    exit 1
fi

# don't overwrite output file
if [[ -f "${OUTFILE}" ]]; then
    echo "Output file '${OUTFILE}' exists, exiting."
    exit
fi


# 1st sed: append char (in this case the command) to linenums
# 2nd sed: execute command
if [ "${REV}" = true ]; then
    # write only lines that are NOT in LINENUMS to OUTFILE
    sed 's/$/d/' "${LINENUMS}" | sed -f - "${INFILE}" >> "${OUTFILE}"
else
    # write only the lines in LINENUMS to OUTFILE
    sed 's/$/p/' "${LINENUMS}" | sed -n -f - "${INFILE}" >> "${OUTFILE}"
fi
