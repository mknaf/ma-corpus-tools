#!/bin/bash

usage() {
cat << EOF
Usage: $( basename $0 ) -s HAYSTACK -w NEEDLES

Read words from NEEDLES and look for them in HAYSTACK.
Write to stdout the linenumbers in HAYSTACK that contain a NEEDLE.

  -h            show this help message and exit
  -s HAYSTACK
                file to search the lines in
  -n NEEDLES
                file to read the searchterms from
EOF
}

INFILE=
NEEDLES=
TEMPDIR=$(mktemp -d)

while getopts "hs:n:o:" opt; do
    case ${opt} in
        h)
            usage
            exit 1
            ;;
        s)
            INFILE="${OPTARG}"
            ;;
        n)
            NEEDLES="${OPTARG}"
            ;;
        ?)
            usage
            exit
            ;;
    esac
done

if [[ -z "${INFILE}" ]] || [[ -z "${NEEDLES}" ]]; then
    usage
    exit 1
fi


# for all the words in the wordlist
while read word; do
    # look for the word in the infile
    # and keep lines in a (tmp) file named after the word
    grep --no-messages \
        --line-number \
        --word-regexp \
        "${word}" "${INFILE}" > "${TEMPDIR}"/"${word}".txt
done < "${NEEDLES}"


# get the linenumbers from all the files, sort and uniq them
cat "${TEMPDIR}"/* | awk -F: '{print $1}' | sort --numeric-sort | uniq

# remove tmp files
rm -rf "${TEMPDIR}"
