#!/bin/bash

IN="EMEA.shuf.de-en.de.test.tok"
OUT="EMEA.shuf.de-en.en.test.tok"

for file in $@; do
    ~/magister/moses/mosesdecoder/bin/moses -f "${file}" -threads 3 < ~/magister/corpus/EMEA/"${IN}" > "${OUT}"."${file}" &
done
